import oscP5.*;
import netP5.*;

int ballcount = 450; 
float ballsize = 2.5;
float ballspd = 2.1;
float frc = 0.998;
float[]xspd = new float[ballcount];
float[]yspd = new float[ballcount];
float[]xpos = new float[ballcount];
float[]ypos = new float[ballcount];
float[]wdth = new float[ballcount];
float[]hit = new float[ballcount];
int[] r = new int[ballcount];
int[] g = new int[ballcount];
int[] b = new int[ballcount];
int[] alp = new int[ballcount];
float _x, _y;

OscP5 oscP5;


void setup() {
  size(displayWidth, displayHeight);  // size always goes first!
  /*if (frame != null) {
   frame.setResizable(true);
   }*/
  background(255);
  frameRate(30);
  smooth();

  for (int i = 0; i<ballcount; i++) {
    xspd[i] = random(-ballspd, ballspd);
    yspd[i] = random(-ballspd, ballspd);

    wdth[i] = random(0.5*ballsize, 0.8*ballsize);
    hit[i] = wdth[i];

    xpos[i] = random(10, width*0.99);
    ypos[i] = random(10, height*0.99);
    r[i] = int(random(155, 255));
    g[i] = int(random(55, 155));
    b[i] = int(random(155, 255));
    alp[i] = int(random(200, 255));
  }

  oscP5 = new OscP5(this, 10000);
}

void oscEvent(OscMessage theOscMessage) {
  _x = theOscMessage.get(0).floatValue();
  _y = theOscMessage.get(1).floatValue();
}

void draw() {
  float m = millis() * 0.001;
  //frc *= 0.998 / (_x*0.1);
  for (int i=0; i <ballcount; i++) {
    fill(r[i], g[i], b[i], alp[i]);
    noStroke();
    //fill(random(0, 60), random(155, 255), random(200, 255), random(80, 155));
    ellipse(xpos[i], ypos[i], wdth[i], hit[i]);
    xpos[i]+= xspd[i] * noise(sin(_x)) * _x;
    ypos[i]+= yspd[i] * noise(cos(_x)) * _x;  
    xspd[i] *= (frc);
    yspd[i] *= (frc);
    if (xpos[i]+wdth[i]/2 >= width || xpos[i]<= wdth[i]/2) {
      xspd[i]*=-1;
    }
    if (ypos[i]+hit[i]/2 >= height || ypos[i]<= hit[i]/2) {
      yspd[i]*=-1;
    }

    float mousedis;
    for (int k = 0; k<i; k++) {
      fill(r[k], g[k], b[k], alp[k]);
      mousedis = dist(xpos[i], ypos[i], xpos[k], ypos[k]);
      if (mousedis < 10) {
        xspd[i] += (xpos[k] - xpos[i])*0.01;
        yspd[i] += (ypos[k] - ypos[i])*0.01;
      }
      if (mousedis > 55 && mousedis < 11) {
        xspd[i] += (xpos[k] + xpos[i])*0.045;
        yspd[i] += (ypos[k] + ypos[i])*0.045;
      }
      float pdis = dist(xpos[i], ypos[i], xpos[k], ypos[k]);
      if (pdis < 45 && pdis > 5) {
        strokeWeight(1.5);
        stroke(r[i], g[i], b[i], alp[i]);
        line(xpos[i], ypos[i], xpos[k], ypos[k]);
      }
    }
    if (dist(xpos[i], ypos[i], mouseX, mouseY) < 25) {
      xspd[i]+= (xpos[i] - mouseX)*0.25;
      yspd[i]+= (ypos[i] - mouseY)*0.25;
    }
  }
  //text(_x, 5, 10);
  fill(255, 255, 255, 10 - _x);
  rect(0,0,width,height);
//background(255, 255, 255, 10 - _x);
}
/*void mousePressed () {
 for (int i=0; i <ballcount; i++) {
 if (dist(xpos[i], ypos[i], mouseX, mouseY) < 96) {
 xspd[i]+= (xpos[i] - mouseX)*0.5;
 yspd[i]+= (ypos[i] - mouseY)*0.5;
 }
 }
 frc = 1.0;
 }*/
void mousePressed() {
  background(255);
  frc = 1.0;
}
