int ballcount = 550; 
int ballsize = 10;
float ballspd = 1.5;
float frc = 0.99888;
float[]xspd = new float[ballcount];
float[]yspd = new float[ballcount];
float[]xpos = new float[ballcount];
float[]ypos = new float[ballcount];
float[]wdth = new float[ballcount];
float[]hit = new float[ballcount];
float m = millis();
void setup() {
  size(displayWidth, displayHeight);  // size always goes first!
  /*if (frame != null) {
    frame.setResizable(true);
  }*/
  background(255);
  frameRate(60);
  smooth();

  for (int i = 0; i<ballcount; i++) {
    xspd[i] = random(-ballspd, ballspd);
    yspd[i] = random(-ballspd, ballspd);

    wdth[i] = random(0.2*ballsize, ballsize);
    hit[i] = wdth[i];

    xpos[i] = random(10, width*0.99);
    ypos[i] = random(10, height*0.99);
  }
}

void draw() {
  background(0);
  for (int i=0; i <ballcount; i++) {
    if (m > 10000) {
      frc = 0.999994;
    }
    if (m > 15000) {
      frc = 1.0;
    }
    noStroke();
    fill(random(0, 60), random(155, 255), random(200, 255), random(80, 155));
    ellipse(xpos[i], ypos[i], wdth[i], hit[i]);

    if (xpos[i]+wdth[i]/2 >= width || xpos[i]<= wdth[i]/2) {
      xspd[i]*=-1;
    }
    if (ypos[i]+hit[i]/2 >= height || ypos[i]<= hit[i]/2) {
      yspd[i]*=-1;
    }

    float mousedis;
    for (int k = 0; k<ballcount; k++) {
      mousedis = dist(xpos[i], ypos[i], xpos[k], ypos[k]);
      if (mousedis < 20) {
        xspd[i] += (xpos[i] - xpos[k])*0.005;
        yspd[i] += (ypos[i] - ypos[k])*0.005;
      }
      if (mousedis > 55 && mousedis < 21) {
        xspd[i] += (xpos[i] + xpos[k])*0.000005;
        yspd[i] += (ypos[i] + ypos[k])*0.00005;
      }
    }
    xpos[i]+= xspd[i] + (random(-1, 1)*(0.8*noise(m*0.1)));
    ypos[i]+= yspd[i] + (random(-1, 1)*(0.8*noise(m*0.1)));  
    xspd[i] *= frc;
    yspd[i] *= frc;
    for (int j = 0; j< i; j++) {
      float dis = 55;
      float pdis = dist(xpos[i], ypos[i], xpos[j], ypos[j]);
      if (pdis < dis && pdis >= 21) {
        strokeWeight(1.1);
        stroke(random(0, 60), random(155, 255), random(200, 255), 65);
        line(xpos[i], ypos[i], xpos[j], ypos[j]);
      }
      if (pdis < 20 && pdis > 8) {
        strokeWeight(1.1);
        stroke(random(0, 60), random(155, 255), random(200, 255), 200);
        line(xpos[i], ypos[i], xpos[j], ypos[j]);
      }
    }
    if (dist(xpos[i], ypos[i], mouseX, mouseY) < 20) {
      xpos[i]+= xspd[i] - 0.09*mouseX;
      ypos[i]+= yspd[i] - 0.09*mouseY;
    }
  }
}

